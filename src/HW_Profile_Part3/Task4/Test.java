package HW_Profile_Part3.Task4;

import java.util.*;

public class Test {

    interface A {}
    interface B {}
    interface C extends A {}

    class D implements B {}
    class E extends D implements C {}

    public static void main(String[] args) {
        Set<Class<?>> interfaces = getAllInterfaces(E.class);
        for (Class<?> cls : interfaces)
            System.out.print(cls.getSimpleName() + " ");
    }

    public static Set<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();

        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }

        List<Class<?>> result = interfaces;

        for (int i = 0; i < interfaces.size(); i++) {
            result.addAll(Arrays.asList(interfaces.get(i).getInterfaces()));
        }

        return new HashSet<>(result);
    }
}
