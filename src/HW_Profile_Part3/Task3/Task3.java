package HW_Profile_Part3.Task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Task3 {
    public static void main(String[] args) {
        Class<APrinter> cls = APrinter.class;

        try {
            Constructor<APrinter> constructor = cls.getDeclaredConstructor();
            APrinter result = constructor.newInstance();

            Method method = cls.getMethod("print", int.class);
            method.invoke(result, 1);
            method.invoke(result, -5);
            method.invoke(result, 0);
        } catch (NoSuchMethodException e) {
            System.out.println("ERROR: метод не найден");
        } catch (IllegalAccessException e) {
            System.out.println("ERROR: метод не имеет доступа к определению класса");
        } catch (InvocationTargetException e) {
            System.out.println("ERROR: невозможно вызвать метод");
        } catch (InstantiationException e) {
            System.out.println("ERROR: попытка создать объект абстрактного класса или интерфейса");
        }
    }
}