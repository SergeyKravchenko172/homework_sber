package HW_Profile_Part3.Task2;

import HW_Profile_Part3.Task1.IsLike;

public class Task2 {
    public static void main(String[] args) {
        writeIsLike(Test.class);
        writeIsLike(Test2.class);
    }

    public static void writeIsLike(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            System.out.println("Annotation IsLike in class " + cls.getSimpleName() + " not available");
            return;
        }

        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("Annotation IsLike in class " + cls.getSimpleName() + " is available");
        System.out.println("Value: " + isLike.bool());
    }
}
