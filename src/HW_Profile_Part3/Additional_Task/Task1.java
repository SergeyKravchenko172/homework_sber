package HW_Profile_Part3.Additional_Task;

public class Task1 {
    public static void main(String[] args) {
        String string1 = "(()()())";
        String string2 = ")(";
        String string3 = "(()";
        String string4 = "((()))";
        String string5 = "";

        System.out.println(isCorrectSequence(string1));
        System.out.println(isCorrectSequence(string2));
        System.out.println(isCorrectSequence(string3));
        System.out.println(isCorrectSequence(string4));
        System.out.println(isCorrectSequence(string5));
    }

    /*
    public static boolean isCorrectSequence(String sequence) {

        if (sequence.equals("")) {
            return true;
        } else if (sequence.charAt(0) == ')')
            return false;

        int countOpen = 0;
        int countClose = 0;

        for (String str : sequence.split("")) {
            if (str.equals("("))
                countOpen++;
            if (str.equals(")"))
                countClose++;
        }

        return countOpen == countClose;
    }
     */

    public static boolean isCorrectSequence(String sequence) {

        if (sequence.equals("")) {
            return true;
        } else if (sequence.charAt(0) == ')')
            return false;

        while (sequence.length() > 1) {
            sequence = sequence.replaceAll("\\(\\)", "");
        }
        return sequence.length() == 0;
    }
}
