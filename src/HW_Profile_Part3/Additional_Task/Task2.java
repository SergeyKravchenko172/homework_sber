package HW_Profile_Part3.Additional_Task;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Task2 {
    /*
    public static void main(String[] args) {

    }

    public static boolean isCorrectSequence(String sequence) {
        if (sequence.equals(""))
            return true;
        else if (sequence.charAt(0) == ')' || sequence.charAt(0) == ']' || sequence.charAt(0) == '}')
            return false;

        int countOpen1 = 0; //(
    }
     */
    public static void main(String[] args) {

        System.out.println(isCorrectSequence("{()[]()}"));
        System.out.println(isCorrectSequence("{)(}"));
        System.out.println(isCorrectSequence("[}"));
        System.out.println(isCorrectSequence("[{(){}}][()]{}"));
        System.out.println(isCorrectSequence("(()()())"));
        System.out.println(isCorrectSequence("(()"));

    }

    public static boolean isCorrectSequence(String sequence) {
        Map<Character, Character> openClosePair = new HashMap<>();
        openClosePair.put(')', '(');
        openClosePair.put('}', '{');
        openClosePair.put(']', '[');
        Stack<Character> stack = new Stack<>();

        try {
            for(char ch : sequence.toCharArray()) {
                if(openClosePair.containsKey(ch)) {
                    if(stack.pop() != openClosePair.get(ch)) {
                        return false;
                    }
                } else if(openClosePair.containsValue(ch)) {
                    stack.push(ch);
                }
            }
            return stack.isEmpty();
        } catch (EmptyStackException e) {
            return false;
        }
    }
}
