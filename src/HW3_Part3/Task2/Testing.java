package HW3_Part3.Task2;

public class Testing {
    public static void main(String[] args) {
        // Создаем 4 элемента мебели
        Furniture furniture1 = new Furniture("Стол");
        Furniture furniture2 = new Furniture("Стул");
        Furniture furniture3 = new Furniture("Табуретка");
        Furniture furniture4 = new Furniture("Кресло");

        // создаем цех
        BestCarpenterEver carpenter = new BestCarpenterEver();

        // проверяем, может ли
        // цех починить мебель
        // и выводим результат
        System.out.println(carpenter.isFix(furniture1));
        System.out.println(carpenter.isFix(furniture2));
        System.out.println(carpenter.isFix(furniture3));
        System.out.println(carpenter.isFix(furniture4));
    }
}