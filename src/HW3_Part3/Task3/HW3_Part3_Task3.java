package HW3_Part3.Task3;

import java.util.*;

public class HW3_Part3_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить количество столбцов
        int n = console.nextInt();

        // Получить количество строк
        int m = console.nextInt();

        // Создаем список, хранящий индексы
        ArrayList<Integer> list = new ArrayList<>();

        // Добавляем в список числа
        // от 0 до большего среди
        // значения строк и столбцов
        if (m > n){
            for (int i = 0; i < m; i++)
                list.add(i);
        } else {
            for (int i = 0; i < n; i++)
                list.add(i);
        }

        // Выводим матрицу, в которой
        // каждый элемент имеет
        // значение суммы своих индексов
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++)
                System.out.print(list.get(i) + list.get(j) + " ");
            System.out.println();
        }
    }
}