package HW3_Part3.Task1;

public abstract class Animals{

    // Метод, выводящий, что животное ест
    public void eat(){
        System.out.println("Ест");
    }

    // Метод, выводящий, что животное спит
    public void sleep(){
        System.out.println("Спит");
    }

    // Абстрактный метод, выводящий, как
    // животные рождаются
    public abstract void wayOfBirth();
}