package HW3_Part3.Task1;

class Eagle extends Bird {
    Eagle(){

    }

    // Переопределенный метод,
    // показывающий, как рождаются
    // орлы
    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }

    // Переопределенный метод,
    // показывающий, как летают
    // орлы
    @Override
    public void fly() {
        System.out.println("Орел летает быстро");
    }
}