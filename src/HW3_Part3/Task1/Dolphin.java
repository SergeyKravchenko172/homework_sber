package HW3_Part3.Task1;

class Dolphin extends Mammal {
    Dolphin(){

    }

    // Переопределенный метод,
    // показывающий, как рождаются
    // дельфины
    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }

    @Override
    public void fly() {

    }

    // Переопределенный метод,
    // показывающий, как плавают
    // дельфины
    @Override
    public void swim() {
        System.out.println("Дельфин плавает быстро");
    }
}