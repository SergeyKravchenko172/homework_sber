package HW3_Part3.Task1;

class GoldFish extends Fish {
    GoldFish(){

    }

    // Переопределенный метод,
    // показывающий, как рождаются
    // золотые рыбки
    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }

    // Переопределенный метод,
    // показывающий, как плавают
    // золотые рыбки
    @Override
    public void swim() {
        System.out.println("Золотая рыбка плавает медленно");
    }
}