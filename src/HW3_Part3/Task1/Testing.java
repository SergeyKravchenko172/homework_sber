package HW3_Part3.Task1;

public class Testing {
    public static void main(String[] args) {
        // Создаем 4-х животных
        Bat bat = new Bat();
        Eagle eagle = new Eagle();
        Dolphin dolphin = new Dolphin();
        GoldFish goldFish = new GoldFish();

        // Выводим, какими свойствами
        // обладает летучая мышь
        System.out.println("Летучая мышь");
        bat.eat();
        bat.sleep();
        bat.wayOfBirth();
        bat.fly();
        System.out.println();

        // Выводим, какими свойствами
        // обладает дельфин
        System.out.println("Дельфин");
        dolphin.eat();
        dolphin.sleep();
        dolphin.wayOfBirth();
        dolphin.swim();
        System.out.println();

        // Выводим, какими свойствами
        // обладает золотая рыбка
        System.out.println("Золотая рыбка");
        goldFish.eat();
        goldFish.sleep();
        goldFish.wayOfBirth();
        goldFish.swim();
        System.out.println();

        // Выводим, какими свойствами
        // обладает орел
        System.out.println("Орел");
        eagle.eat();
        eagle.sleep();
        eagle.wayOfBirth();
        eagle.fly();
    }
}