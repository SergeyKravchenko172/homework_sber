package HW3_Part3.Task1;

class Bat extends Mammal{
    Bat(){

    }

    // Переопределенный метод,
    // показывающий, как рождаются
    // летучие мыши
    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }

    // Переопределенный метод,
    // показывающий, как летают
    // летучие мыши
    @Override
    public void fly() {
        System.out.println("Летучая мышь летает медленно");
    }

    @Override
    public void swim() {

    }
}