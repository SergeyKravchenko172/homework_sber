package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем два натуральных числа
        int m = console.nextInt();
        int n = console.nextInt();

        // Выводим остаток от деления
        System.out.println(mod(m, n));
    }
    public static int mod(int m, int n){
        int integerDiv = m / n; // Результат целочисленного деления
        int integerMult = n * integerDiv; // Результат умножения числа n на
        // результат целочисленного деления
        int result = m - integerMult; // Разница между числом m и результата умножения
        // (остаток от деления)
        // Возвращаем результат
        return result;
    }
}