package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем два положительных целых числа
        int m = console.nextInt();
        int n = console.nextInt();

        int sum = 0; // сумма чисел между m и n
        // Вычисляем сумму чисел между m и n
        for (int i = m; i <= n; i++){
            sum += i;
        }
        // Выводим сумму
        System.out.println(sum);
    }
}