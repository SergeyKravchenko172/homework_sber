package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task10 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем высоту елочки
        int n = console.nextInt();

        // Цикл, формурующий елку
        for (int j = 0; j < n; j++){
            for (int i = 0; i < n * 2 - 1; i++){
                if (i >= (n * 2 - 1) / 2 - j && i <= (n * 2 - 1) / 2 + j){
                    System.out.print("#");
                }
                else if (i <= (n * 2 - 1) / 2 + j) System.out.print(" ");
            }
            System.out.println("");
        }
        for (int i = 0; i < n * 2 - 1; i++){
            if (i == (n * 2 - 1) / 2) System.out.print("|");
            else System.out.print(" ");
        }
    }
}