package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем строку
        String s = console.nextLine();

        int countChar = 0; // количество символов
        // Переберем символы в строке, минуя пробелы
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) != ' ') ++countChar;
        }
        System.out.println(countChar);
    }
}