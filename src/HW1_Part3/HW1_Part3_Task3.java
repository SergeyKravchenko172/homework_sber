package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем два положительных целых числа
        int m = console.nextInt();
        int n = console.nextInt();

        int sum = 0; // сумма чисел m в степени от 1 до n

        // Вычисляем сумму чисел
        for (int i = 1; i <= n; i++){
            sum += Math.pow(m, i);
        }
        // Выводим сумму
        System.out.println(sum);
    }
}