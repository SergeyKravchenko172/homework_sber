package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task6 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем положительное число n
        int n = console.nextInt();

        int nominalEight = 0; // количество купюр с номиналом 8
        int nominalFour = 0; // количество купюр с номиналом 4
        int nominalTwo = 0; // количество купюр с номиналом 2
        int nominalOne = 0; // количество купюр с номиналом 1
        while (n >= 8){
            nominalEight += 1;
            n -= 8;
        }
        while (n >= 4 && n < 8){
            nominalFour += 1;
            n -= 4;
        }
        while (n >= 2 && n < 4){
            nominalTwo += 1;
            n -= 2;
        }
        if (n == 1){
            nominalOne += 1;
            n -= 1;
        }
        System.out.println(nominalEight + " " +
                nominalFour + " " +
                nominalTwo + " " +
                nominalOne);
    }
}