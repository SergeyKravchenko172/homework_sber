package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        int a; // число, которое будет подаваться в цикле
        int countNegative = 0; // количество отрицательных чисел

        // Вводим числа, пока не введем положительное
        do {
            a = console.nextInt();
            if (a < 0) countNegative += 1;
        } while (a < 0);
        System.out.println(countNegative);
    }
}