package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем целое число n
        int n = console.nextInt();

        // Получаем цело число p
        int p = console.nextInt();

        int sum = 0; // сумма чисел a1, a2, ..., an, которые строго больше p

        // Вводим числа a1, a2, ..., an
        for (int i = 0; i < n; ++i){
            int a = console.nextInt();
            if (a > p) sum += a;
        }
        System.out.println(sum);
    }
}