package HW1_Part3;

import java.util.Scanner;

public class HW1_Part3_Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем натуральное число n
        int n = console.nextInt();

        String str_n = "" + n; // число n с типом String

        // Выводим str_n (цифры числа n)
        for (int i = 0; i < str_n.length(); i++){
            System.out.println(str_n.charAt(i));
        }
    }
}