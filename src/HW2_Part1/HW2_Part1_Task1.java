//package HW2_Part1;

import java.util.Scanner;

public class HW2_Part1_Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Создать массив длины N
        double[] array = new double[N];

        double averageValue = 0.0; // среднее значение

        // Заполнить массив
        for (int i = 0; i < N; i++){
            array[i] = console.nextDouble();
            averageValue += array[i];
        }
        averageValue /= N;

        // Вывести среднее значение чисел в массиве
        System.out.println(averageValue);
    }
}
