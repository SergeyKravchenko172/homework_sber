package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявляем массив
        String[] array = new String[N];

        // Заполняем массив
        for (int i = 0; i < N; i++)
            array[i] = console.next();

        String result = ""; // найденный дубликат

        // Поиск дубликата
        for (int i = 0; i < N; i++){
            if (result.length() > 0)
                break;
            for (int j = i + 1; j < N; j++){
                if (array[i].equals(array[j])) {
                    result = array[i];
                    break;
                }
            }
        }
        // Выводим дубликат
        System.out.println(result);
    }
}