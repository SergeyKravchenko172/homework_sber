package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявить массив
        int[] array = new int[N];

        // Заполнить массив
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        // Возвести элементы массива в квадрат
        for (int i = 0; i < N; i++) {
            array[i] *= array[i];
        }

        // Сортировка массива
        for (int i = 0; i < N; i++) {
            for (int j = 1 + i; j < N; j++) {
                int temp = array[i];
                if (temp > array[j]) {
                    int x = array[j];
                    array[i] = x;
                    array[j] = temp;
                }
            }
        }

        // Выводим результат
        for (int x : array)
            System.out.print(x + " ");
    }
}