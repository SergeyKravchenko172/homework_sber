package HW2_Part1;

import java.util.Scanner;

public class HW2_Part1_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявить массив
        int[] array = new int[N];

        // Заполнить массив числами по возрастанию
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        // Получить элемент, который нужно добавить в массив
        int X = console.nextInt();

        int index = 0; // Искомый индекс
        // Поиск индекса вставки для X
        for (int i = 0; i < N; i++){
            if (array[i] <= X) {
                index = i + 1;
                continue;
            }
            else if (array[i] > X)
                break;
        }
        // Вывести индекс вставки элемента X
        System.out.println(index);
    }
}
