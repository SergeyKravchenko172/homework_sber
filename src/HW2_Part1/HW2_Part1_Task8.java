package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявляем массив
        int[] array = new int[N];

        // Заполняем массив
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        // Получить число M
        int M = console.nextInt();

        // Поиск ближайшего к M числа в массиве
        int min = -999999;
        for (int i = 1; i < N; i++){
            if (Math.abs(array[i - 1] - M) > Math.abs(array[i] - M) && array[i] > min)
                min = array[i];
        }
        // Выводим результат
        System.out.println(min);
    }
}