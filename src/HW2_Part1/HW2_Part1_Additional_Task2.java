package HW2_Part1;

import java.util.*;

public class HW2_Part1_Additional_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявить массив
        int[] array = new int[N];

        // Заполнить массив
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        // Возвести элементы массива в квадрат
        for (int i = 0; i < N; i++) {
            array[i] *= array[i];
        }

        // Сортируем массив
        sort(array);

        // Выводим результат
        for (int x : array)
            System.out.print(x + " ");
    }
    public static int[] sort(int[] array) {
        int min, max;
        max = min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            if (array[i] > max) {
                max = array[i];
            }
        }

        return sort(array, min, max);
    }

    static int[] sort(int[] array, int min, int max) {
        // создаем массив счетчиков
        int[] count = new int[max - min + 1];

        // считаем сколько раз встречается каждое число,
        for (int i = 0; i < array.length; i++) {
            // берем нужный счетчик и добавляем к нему +1
            count[array[i] - min]++;
        }

        int idx = 0;
        // пробегаем по всем счетчикам
        // count[i] - показывает сколько раз встречается то или иное число
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                array[idx++] = i + min;
            }
        }

        return array;
    }
}