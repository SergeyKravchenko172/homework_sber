package HW2_Part1;

import java.util.Scanner;

public class HW2_Part1_Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявляем массив
        int[] array = new int[N];

        // Заполняем массив
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        // Получить величину сдвига
        int M = console.nextInt();

        // Совершить сдвиг элементов массива вправо на M
        while (M > 0){
            // Добавить последний эелемент в буфер
            int lastElements = array[N - 1];
            for (int i = 0; i < N; i++){
                int temp = array[i];
                array[i] = lastElements;
                lastElements = temp;
            }
            M--;
        }
        // Выводим сдвинутый массив
        for (int x : array)
            System.out.print(x + " ");
    }
}
