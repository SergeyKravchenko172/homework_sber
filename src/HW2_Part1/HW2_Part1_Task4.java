package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива
        int N = console.nextInt();

        // Объявить массив
        int[] array = new int[N];

        // Заполнить массив числами по возрастанию
        for (int i = 0; i < N; i++)
            array[i] = console.nextInt();

        int count = 1; // количество различных чисел

        // Подсчитать количество различных чисел
        for (int i = 0; i < N - 1; i++) {
            if (array[i] == array[i + 1])
                count++;
            else { // если следующее число перестает быть равным текущему, то выводим результат
                System.out.println(count + " " + array[i]);
                count = 1;
            }
        }
        // Выводим количество для последнего числа
        System.out.println(count + " " + array[N - 1]);
    }
}
