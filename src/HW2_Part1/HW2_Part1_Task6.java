package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task6 {
    public static void main(String[] args) {
        final String[] MORSE = {".-", "-...", ".--", "--.", "-..", ".", "...-",
                "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.",
                "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..",
                "..--", ".-.-"};

        Scanner console = new Scanner(System.in);

        // Получить строку
        String S = console.next();

        // Результирующая строка
        String result = "";

        // Кодировать строку в азбуку Морзе
        for (int i = 0; i < S.length(); i++){
            result += MORSE[S.charAt(i) - 1040] + " ";
        }

        // Вывести результирующую строку
        System.out.println(result);
    }
}