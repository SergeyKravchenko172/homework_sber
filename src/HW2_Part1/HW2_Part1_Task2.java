package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину массива_1
        int N = console.nextInt();

        // Объявить массив_1
        int[] array_1 = new int[N];

        // Заполнить массив_1
        for (int i = 0; i < N; i++)
            array_1[i] = console.nextInt();

        // Получить длину массива_2
        int M = console.nextInt();

        // Объявить массив_2
        int[] array_2 = new int[M];

        // Заполнить массив_2
        for (int i = 0; i < M; i++)
            array_2[i] = console.nextInt();

        // Вывести результат сравнения двух массивов
        System.out.println(Arrays.equals(array_1, array_2));
    }
}
