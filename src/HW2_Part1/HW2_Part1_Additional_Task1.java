package HW2_Part1;

import java.util.*;

public class HW2_Part1_Additional_Task1 {
    static final char[] BIG_LETTERS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                       'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                                       'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    static final char[] LITTLE_LETTERS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                                          'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                                          'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                                          'y', 'z'};
    static final char[] SPECIAL_CHAR = {'_', '*', '-'};

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить длину пароля
        System.out.print("Введите длину желаемого пароля: ");
        int N = console.nextInt();

        // Проверка длины пароля
        while (N < 8){
            System.out.println("Пароль с " + N + " количеством символов" +
                    " небезопасен");
            System.out.print("Попробуйте ввести другую длину: ");
            N = console.nextInt();
        }

        // Выводим сгенерированный пароль
        System.out.println("Сгенерированный пароль: " + generationPassword(N));
    }

    /**
     * Генерация пароля
     */
    static String generationPassword(int N){
        String password = ""; // переменная, которая будет хранить сгенерированный пароль
        for (int i = 0; i < N; i++){
            if (i == 0)
                password += BIG_LETTERS[(int) (Math.random() * 26)];
            else if (i == 1)
                password += LITTLE_LETTERS[(int) (Math.random() * 26)];
            else if (i % 5 == 0)
                password += (int) (Math.random() * 10);
            else if (i % 7 == 0)
                password += SPECIAL_CHAR[(int) (Math.random() * 3)];
            else if ((int) (Math.random() * 2) == 0)
                password += BIG_LETTERS[(int) (Math.random() * 26)];
            else
                password += LITTLE_LETTERS[(int) (Math.random() * 26)];
        }
        return password;
    }
}