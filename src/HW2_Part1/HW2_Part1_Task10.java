package HW2_Part1;

import java.util.*;

public class HW2_Part1_Task10 {
    public static void main(String[] args) {

        // Запускаем игру (метод)
        game();
    }

    /**
     * Игра "Угадай число"
     */
    static void game(){
        Scanner console = new Scanner(System.in);
        System.out.println("Компьютер загадал число от 0 до 1000. Попробуйте угадать");

        // Получить число
        int number = console.nextInt();

        double M = Math.random() * 1000 + 1; // загаданной число

        // Сравнение загаданного числа от числа пользователя
        do {
            if ((int) M < number)
                System.out.println("Это число больше загаданного");
            else
                System.out.println("Это число меньше загаданного");

            number = console.nextInt();
        } while (number != (int) M);
        System.out.println("Победа!");
    }
}