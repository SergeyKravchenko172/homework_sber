package HW_Profile_Part2.Task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    PowerfulSet(){

    }

    /**
     * Возвращает пересечение
     * двух наборов Set
     * @param set1
     * @param set2
     * @param <T>
     * @return
     */
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.retainAll(set2);
        return set;
    }

    /**
     * Возвращает объединение
     * двух наборов Set
     * @param set1
     * @param set2
     * @param <T>
     * @return
     */
    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.addAll(set2);
        return set;
    }

    /**
     * Возвращает элементы первого Set без тех,
     * которые есть также во втором Set
     * @param set1
     * @param set2
     * @param <T>
     * @return
     */
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>();
        set.addAll(set1);
        set.removeAll(set2);
        return set;
    }
}
