package HW_Profile_Part2.Task3;

import java.util.HashSet;
import java.util.Set;

public class Test {
    public static void main(String[] args) {
        // Создаем объект нашего
        // ранее созданного класса
        PowerfulSet powerfulSet = new PowerfulSet();
        // Создаем 2 Set
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();

        // Добавляем значения в первый Set
        set1.add(1);
        set1.add(2);
        set1.add(3);

        // Добавляем значения во второй Set
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        // Выводим результаты
        System.out.println("Set1: " + set1);
        System.out.println("Set2: " + set2);
        System.out.println(powerfulSet.intersection(set1, set2));
        System.out.println(powerfulSet.union(set1, set2));
        System.out.println(powerfulSet.relativeComplement(set1, set2));
    }
}
