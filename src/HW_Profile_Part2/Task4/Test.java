package HW_Profile_Part2.Task4;

import java.util.ArrayList;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        ArrayToMap arrayToMap = new ArrayToMap();

        // Создаем 3 документа
        Document document1 = new Document(1, "test1", 1);
        Document document2 = new Document(2, "test2", 2);
        Document document3 = new Document(3, "test3", 3);

        // Создаем ArrayList, который
        // будет хранить документы
        ArrayList<Document> list = new ArrayList<>();

        // Добавляем документы
        // в ArrayList
        list.add(document1);
        list.add(document2);
        list.add(document3);

        // Выводим ArrayList
        list.forEach(i -> {
            System.out.println(i.id + "-" + i.name + "-" + i.pageCount);
        });

        // Создаем Map и добавляем в него
        // преобразованный ArrayList
        Map<Integer, Document> map = arrayToMap.organizeDocuments(list);

        // Выводим Map
        for (int i = 1; i < map.size() + 1; i++) {
            System.out.println(map.get(i).id + "-" + map.get(i).name + "-" + map.get(i).pageCount);
        }
    }
}
