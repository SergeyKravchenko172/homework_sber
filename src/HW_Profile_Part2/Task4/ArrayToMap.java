package HW_Profile_Part2.Task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ArrayToMap {

    /**
     * Преобразует ArrayList
     * в Map
     * @param documents
     * @return
     */
    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document d : documents) {
            map.put(d.id, d);
        }
        return map;
    }
}