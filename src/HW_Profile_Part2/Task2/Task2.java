package HW_Profile_Part2.Task2;

import java.util.ArrayList;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Объявляем 2 строки, которые
        // будем проверять в программе
        String s = console.nextLine();
        String t = console.nextLine();

        // Создаем 2 списка, которые будут хранить
        // объекты типа Character
        ArrayList<Character> listS = new ArrayList<>();
        ArrayList<Character> listT = new ArrayList<>();

        // Заполняем listS символами строки s
        for (int i = 0; i < s.length(); i++)
            listS.add(s.charAt(i));

        // Заполняем listT символами строки t
        for (int i = 0; i < t.length(); i++)
            listT.add(t.charAt(i));

        // Выводим результат
        // (Проверяем входят ли все элементы одного списка
        // в состав другого списка)
        System.out.println(listS.containsAll(listT) && listT.containsAll(listS));
    }
}
