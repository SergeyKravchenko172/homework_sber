package HW_Profile_Part2.Task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {
        // Создаем ArrayList, хранящий объекты Integer
        // и ArrayList, хранящий объекты String
        ArrayList<Integer> listInteger = new ArrayList<>();
        ArrayList<String> listString = new ArrayList<>();

        // Заполняем ArrayList<Integer>
        listInteger.add(1);
        listInteger.add(3);
        listInteger.add(3);
        listInteger.add(5);
        listInteger.add(1);

        // Заполняем ArrayList<String>
        listString.add("abc");
        listString.add("a");
        listString.add("bc");
        listString.add("abc");
        listString.add("cdef");

        // Выводим набор уникальных значений
        // каждого ArrayList
        System.out.println(listToSet(listInteger));
        System.out.println(listToSet(listString));
    }

    /**
     * Возвращает набор уникальных
     * значений ArrayList
     * @param list
     * @return
     */
    public static <T> Set<T> listToSet(ArrayList<T> list){
        return new HashSet<>(list);
    }
}
