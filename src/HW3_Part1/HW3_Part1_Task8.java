package HW3_Part1;

// ДЗ 3 Часть 1 Номер 8

public class HW3_Part1_Task8 {
    public static void main(String[] args) {
        // Задаем 3 курса
        Atm atm = new Atm(60.3);
        Atm atm1 = new Atm(124.59);
        Atm atm2 = new Atm(0.12);

        // Переводим рубли в доллары по 3м курсам
        System.out.println(atm.rublesToDollars(100));
        System.out.println(atm1.rublesToDollars(1000));
        System.out.println(atm2.rublesToDollars(987));

        // Переводим доллары в рубли по 3м курсам
        System.out.println(atm.dollarsToRubles(60.3));
        System.out.println(atm1.dollarsToRubles(5593));
        System.out.println(atm2.dollarsToRubles(800994));

        // Выводим количество заданных курсов валют
        System.out.println(Atm.printCount());
    }
}

class Atm {
    private static int count; // количество созданных экземпляров
    private double course; // курс валют

    Atm(double course){
        this.course = course;
        count++;
    }

    /**
     * Переводит рубли в доллары
     * @param rubles
     * @return
     */
    public double rublesToDollars(double rubles){
        return rubles * course;
    }

    /**
     * Переводит доллары в рубли
     * @param dollars
     * @return
     */
    public double dollarsToRubles(double dollars){
        return dollars / course;
    }

    /**
     * Выводит количество
     * созданных экземпляров
     * @return
     */
    public static int printCount(){
        return count;
    }
}