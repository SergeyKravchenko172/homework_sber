package HW3_Part1;

// ДЗ 3 Часть 1 Номер 7

public class HW3_Part1_Task7 {
    public static void main(String[] args) {

        System.out.println(TriangleChecker.check(1, 1, 1));
        System.out.println(TriangleChecker.check(2, 3, 1));
        System.out.println(TriangleChecker.check(-1, 0, 1));
        System.out.println(TriangleChecker.check(4, 4, 5));
    }
}

class TriangleChecker {
    /**
     * Проверяет, может ли
     * существовать треугольник
     * с указанными значениями
     * сторон
     * @param side1
     * @param side2
     * @param side3
     * @return
     */
    public static boolean check(int side1, int side2, int side3){
        if (side1 <= 0 || side2 <= 0 || side3 <= 0)
            return false;
        if (side1 + side2 > side3
           && side2 + side3 > side1
           && side1 + side3 > side2)
            return true;
        return false;
    }
}