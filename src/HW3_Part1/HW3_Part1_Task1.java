package HW3_Part1;

// ДЗ 3 Часть 1 Номер 1

class Cat {
    // приватный метод, выводящий "Sleep"
    private void sleep(){
        System.out.println("Sleep");
    }

    // приватный метод, выводящий "Meow"
    private void meow(){
        System.out.println("Meow");
    }

    // приватный метод, выводящий "Eat"
    private void eat(){
        System.out.println("Eat");
    }

    /**
     * публичный метод, вызывающий один
     * из приватных методов случайным
     * образом
      */
    public void status(){
        int i = (int) (Math.random() * 3);
        switch (i) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }
}

public class HW3_Part1_Task1 {
    public static void main(String[] args) {
        // создаем объект типа Cat
        Cat cat = new Cat();

        // вызываем метод status()
        // несколько раз
        cat.status();
        cat.status();
        cat.status();
        cat.status();
    }
}