package HW3_Part1;

// ДЗ 3 Часть 1 Номер 4

public class HW3_Part1_Task4 {
    public static void main(String[] args) {

        // создаем объекты типа TimeUnit
        TimeUnit timeUnit = new TimeUnit(9, 15, 59);
        TimeUnit timeUnit1 = new TimeUnit(13, 15);
        TimeUnit timeUnit2 = new TimeUnit(22);

        // получаем время в 24-часовом формате
        System.out.println(timeUnit.getTime());
        System.out.println(timeUnit1.getTime());
        System.out.println(timeUnit2.getTime());

        // получаем время в 12-часовом формате
        System.out.println(timeUnit.getTwelveTime());
        System.out.println(timeUnit1.getTwelveTime());
        System.out.println(timeUnit2.getTwelveTime());

        // добавляем введенные часы, минуты
        // и секунды к установленному времени
        timeUnit.addTime(5, 20, 30);
        timeUnit1.addTime(1, 12, 1);
        timeUnit2.addTime(8, 6, 15);

        // получаем время в 24-часовом формате
        // после добавления введенного врмени
        System.out.println(timeUnit.getTime());
        System.out.println(timeUnit1.getTime());
        System.out.println(timeUnit2.getTime());

        // получаем время в 12-часовом формате
        // после добавления введенного врмени
        System.out.println(timeUnit.getTwelveTime());
        System.out.println(timeUnit1.getTwelveTime());
        System.out.println(timeUnit2.getTwelveTime());
    }
}

class TimeUnit{
    private int hour; // часы
    private int minute; // минуты
    private int second; // секунды

    // конструктор с заданными часами, минутами и секундами
    TimeUnit(int hour, int minute, int second){
        if (hour < 24 && minute < 60 && second < 60
        && hour >= 0 && minute >= 0 && second >= 0) {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        } else {
            System.out.println("Ошибка. Введены некорректные значения");
        }
    }

    // конструктор с заданными часами и минутами
    TimeUnit(int hour, int minute){
        if (hour < 24 && minute < 60 && hour >= 0
        && minute >= 0) {
            this.hour = hour;
            this.minute = minute;
        } else {
            System.out.println("Ошибка. Введены некорректные значения");
        }
    }

    // конструктор с заданными часами
    TimeUnit(int hour){
        if (hour < 24 && hour >= 0)
            this.hour = hour;
        else
            System.out.println("Ошибка. Введены некорректные значения");
    }

    /**
     * Выводит время в
     * 24-часовом формате
     * @return
     */
    public String getTime(){
        String strHour = "";
        String strMinute = "";
        String strSecond = "";
        if (hour < 10){
            strHour = "0" + hour;
        } else {
            strHour += hour;
        }
        if (minute < 10){
            strMinute = "0" + minute;
        } else {
            strMinute += minute;
        }
        if (second < 10){
            strSecond = "0" + second;
        } else {
            strSecond += second;
        }
        return strHour + ":" + strMinute + ":" + strSecond;
    }

    /**
     * Выводит время в
     * 12-часовом формате
     * @return
     */
    public String getTwelveTime(){
        if (hour > 12) {
            String strHour = "";
            String strMinute = "";
            String strSecond = "";
            if (hour < 10){
                strHour = "0" + (hour - 12);
            } else {
                strHour += (hour - 12);
            }
            if (minute < 10){
                strMinute = "0" + minute;
            } else {
                strMinute += minute;
            }
            if (second < 10){
                strSecond = "0" + second;
            } else {
                strSecond += second;
            }
            return strHour + ":" + strMinute + ":" + strSecond;
        }
        return getTime() + " am";
    }

    /**
     * Добавляет ко времени
     * введенные часы, минуты и секунды
     * @param hour
     * @param minute
     * @param second
     */
    public void addTime(int hour, int minute, int second){
        // проверка на отрицательные числа
        if (hour >= 0 && minute >= 0 && second >= 0){
            second += this.second;
            this.second = second % 60;

            minute += this.minute + second / 60;
            this.minute = minute % 60;

            hour += this.hour + minute / 60;
            this.hour = hour % 24;

        } else {
            System.out.println("Ошибка. Введены отрицательные значения");
        }
    }
}