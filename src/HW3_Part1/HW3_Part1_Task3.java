package HW3_Part1;

// ДЗ 3 Часть 1 Номер 3

import java.util.*;

public class HW3_Part1_Task3 {
    public static void main(String[] args) {

        StudentService studentService = new StudentService();

        // Объявляем массив студентов
        // (массив типа Student)
        Student[] student = new Student[3];

        student[0] = new Student();
        student[1] = new Student();
        student[2] = new Student();

        // Заполняем информацию про первого студента
        student[0].setName("Ivan");
        student[0].setSurname("Ivanov");
        student[0].setGrades(new int[] {5, 3, 4, 5, 4, 5, 2});

        // Заполняем информацию про второго студента
        student[1].setName("Petr");
        student[1].setSurname("Petrov");
        student[1].setGrades(new int[] {5, 5, 5, 3, 5,});

        // Заполянем информацию про третьего студента
        student[2].setName("Boris");
        student[2].setSurname("Borisov");
        student[2].setGrades(new int[] {4, 4, 5, 5, 5, 5, 4});

        // Выводим студента
        // с большим максимальным
        // баллом
        System.out.println(studentService.bestStudent(student));

        // Сортируем студентов в
        // алфавитном порядке и
        // выводим результат
        studentService.sortBySurname(student);
    }
}

class StudentService {
    /**
     * Метод, который выводит
     * лучшего студента по
     * показателям среднего балла
     * @param student
     * @return
     */
    public String bestStudent(Student[] student){
        double max = -1.0;
        int index = -1;

        // Поиск максимального среднего балла
        for (int i = 0; i < student.length; i++){
            if (student[i].averageGrades() > max){
                max = student[i].averageGrades();
                index = i;
            }
        }
        return student[index].getName() + " " + student[index].getSurname();
    }

    public void sortBySurname(Student[] student){
        String[] surname = new String[student.length];
        for (int i = 0; i < student.length; i++) {
            surname[i] = student[i].getSurname();
        }
        Arrays.sort(surname);
        System.out.println(Arrays.toString(surname));
    }
}