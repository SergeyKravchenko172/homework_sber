package HW3_Part1;

// ДЗ 3 Часть 1 Номер 2

import java.util.Arrays;

class Student {
    private String name; // имя студента
    private String surname; // фамилия студента
    private int[] grades = new int[10]; // последние 10 оценок студента
    private int numberOfGrades; // подсчет оценок

    /**
     получить имя студента
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     получить фамилию студента
     */
    public void setSurname(String surname){
        this.surname = surname;
    }

    /**
     * получить последние 10 оценок студента
      */
    public void setGrades(int[] score){
        for (int i = 0; i < score.length; i ++)
            addGrades(score[i]);
    }

    /**
     вывести имя студента
     */
    public String getName(){
        return name;
    }

    /**
     * вывести фамилию студента
      */
    public String getSurname(){
        return surname;
    }

    /**
     * вывести последние 10 оценок студента
      */
    public String getGrades(){
        int[] result = new int[numberOfGrades]; // массив, который будет хранить полученные оценки
        for (int i = 0; i < grades.length; i++){
            if (grades[grades.length - i - 1] != 0)
                result[numberOfGrades - i - 1] = grades[grades.length - i - 1];
        }
        return Arrays.toString(result);
    }

    /**
     * добавить новую оценку
     * в grades
     */
    public void addGrades(int score){
        // сместить оценки влево
        for (int i = 1; i < grades.length; i++)
            grades[i - 1] = grades[i];
        // добавить новую оценку в конец массива
        grades[grades.length - 1] = score;
        numberOfGrades++;
    }

    /**
     * вывести средний балл студента
     */
    public double averageGrades(){
        double sum = 0; // сумма оценок
        for (int x : grades)
            sum += x;
        return sum / numberOfGrades;
    }


}

public class HW3_Part1_Task2 {
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("Eugene"); // присваеиваем имя
        student.setSurname("Zemlyakov"); // присваиваем фамилию

        // добавим 5 последних оценок
        // студента
        student.setGrades(new int[]{5, 5, 5, 4, 5});

        // добавим еще одну оценку
        student.addGrades(5);

        System.out.println(student.getName()); // выводим имя студента
        System.out.println(student.getSurname()); // выводим фамилию студента
        System.out.println(student.getGrades()); // выводим 10 последних оценок студента
        System.out.println(student.averageGrades()); // выводим средний балл студента
    }
}