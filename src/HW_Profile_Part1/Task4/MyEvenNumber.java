package HW_Profile_Part1.Task4;

public class MyEvenNumber {
    private int n = 2; // значение n по умолчанию

    // Создаем конструктор класса, который
    // в случае создания экземпляра с нечетным
    // числом выбрасывает исключение
    MyEvenNumber(int n) throws NotIsEvenNumberException{
        if (n % 2 == 0){
            this.n = n;
            System.out.println("Экземпляр класса со значением " + n + " успешно создан");
        }
        else
            throw new NotIsEvenNumberException();
    }
}
