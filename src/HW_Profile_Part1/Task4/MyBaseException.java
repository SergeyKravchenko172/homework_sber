package HW_Profile_Part1.Task4;

// Создаем базовое исключение
public class MyBaseException extends RuntimeException{

    public MyBaseException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
