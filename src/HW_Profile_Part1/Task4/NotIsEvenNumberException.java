package HW_Profile_Part1.Task4;

// Создаем класс-исключение
// "не является четным числом"
public class NotIsEvenNumberException extends MyBaseException{

    public NotIsEvenNumberException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    public NotIsEvenNumberException(){
        // Сообщаем об ошибке
        super("Переданное число не является четным");
    }
}
