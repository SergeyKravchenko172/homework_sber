package HW_Profile_Part1.Task4;

public class Test {
    public static void main(String[] args) {
        try {
            // Создаем экземпляр класса
            // MyEvenNumber с значением 4
            MyEvenNumber number_1 = new MyEvenNumber(4);

            // Создаем экземпляр класса
            // MyEvenNumber со значением 5
            MyEvenNumber number_2 = new MyEvenNumber(5);
        } catch (NotIsEvenNumberException ex){
            ex.getMessage();
        }
    }
}
