package HW_Profile_Part1.Task2;

// Создаем непроверяемое исключение,
// наследуемое от класса RuntimeException,
// так как класс RuntimeException является
// непроверяемым исключением
public class MyUncheckedException extends RuntimeException{

    MyUncheckedException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
