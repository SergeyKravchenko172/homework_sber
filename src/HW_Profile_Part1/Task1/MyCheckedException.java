package HW_Profile_Part1.Task1;

// Создаем проверяемое исключение,
// наследуемое от класса Exception,
// так как класс Exception является
// проверяемым исключением
public class MyCheckedException extends Exception{

    MyCheckedException(String errorMessage){
        // Передаем сообщение об ошибки
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
