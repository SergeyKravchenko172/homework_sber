package HW_Profile_Part1.Additional_Task;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Размер массива
        int n = console.nextInt();

        // Массив размеромм n
        int[] array = new int[n];

        // Инициализация массива
        for (int i = 0; i < n; i++)
            array[i] = console.nextInt();

        int highMax = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        // Поиск двух максимальных значений
        for (int x : array) {
            if (x > highMax){
                secondMax = highMax;
                highMax = x;
            } else if (x > secondMax)
                secondMax = x;
        }

        System.out.println(highMax + " " + secondMax);
    }
}
