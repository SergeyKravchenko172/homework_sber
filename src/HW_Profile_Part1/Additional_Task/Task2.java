package HW_Profile_Part1.Additional_Task;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Размер массива
        int n = console.nextInt();

        // Объявление массива
        int[] array = new int[n];

        // Инициализация массива
        for (int i = 0; i < n; i++)
            array[i] = console.nextInt();

        // Целое число, которое потребуется найти
        int p = console.nextInt();

        // Вывод результата бинарного поиска
        System.out.println(binarySearch(array, p));
    }

    /**
     * Бинарный поиск
     * @param array
     * @param p
     * @return
     */
    public static int binarySearch(int[] array, int p){
        // индекс медианного элемента
        int mid = array.length / 2;
        // индекс макимального по значению элемента
        int high = array.length - 1;
        // индекс минимального по значению элемента
        int low = 0;

        while(low <= high){
            if (p < array[mid]){
                high = mid - 1;
                mid = low + (high - low) / 2;
            } else if (p > array[mid]){
                low = mid + 1;
                mid = low + (high - low) / 2;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
