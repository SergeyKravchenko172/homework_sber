package HW_Profile_Part1.Task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class Task3 {
    // Путь нахождения файлов
    private static final String PKG_DIRECTORY = "C:/Users/zemly/Desktop";
    // Название файла чтения
    private static final String INPUT_FILE_NAME = "input.txt";
    // Название файла записи
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) throws IOException {
        // Проверка исключения
        // на ввод-вывод
        try {
            readAndWriteToUpperCase();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Записывает текст из певрого файла
     * во второй, меняя буквы латинского
     * алфавита на заглавные
     * @throws IOException
     */
    public static void readAndWriteToUpperCase() throws IOException{
        Scanner console = new Scanner(new File(PKG_DIRECTORY + "/" + INPUT_FILE_NAME));
        Writer wr = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);


        try (console; wr) {
            String str = "";

            if (console.hasNextLine()) {

                // Записываем в строку текст из первого файла
                str = console.nextLine();

                // Поочередно проверяем, является ли
                // символ строки прописной буквой
                // латинского алфавита
                for (int i = 0; i < str.length(); i++) {
                    // Если является, то делаем символ заглавным и записываем его во второй файл
                    if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
                        wr.write(Character.toUpperCase(str.charAt(i)));
                    // Если не является, то записываем символ без изменений
                    else
                        wr.write(str.charAt(i));
                }
            }
        }
    }
}
