package HW_Profile_Part1.Task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class FormValidator {
    // Перечисление полов
    enum Gender{
        Female, Male
    }

    // Безаргументный конструктор
    FormValidator(){

    }

    /**
     * Проверяет корректность
     * полученного имени
     * @param str
     * @throws InvalidNameException
     */
    public void checkName(String str) throws InvalidNameException{
        if (str.length() >= 2 && str.length() <= 20 && Character.isUpperCase(str.charAt(0)))
            System.out.println("Имя корректно");
        else
            throw new InvalidNameException();
    }

    /**
     * Проверяет коррекность
     * полученной даты рождения
     * @param str
     * @throws InvalidBirthdateException
     */
    public void checkBirthdate(String str) throws InvalidBirthdateException{
        Date birthdate = new Date(1);
        Date minBirthdate = new Date(1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            birthdate = formatter.parse(str);
            minBirthdate = formatter.parse("01.01.1900");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date maxBirthdate = new Date();

        if (birthdate.after(minBirthdate) && birthdate.before(maxBirthdate))
            System.out.println("Дата рождения корректна");
        else
            throw new InvalidBirthdateException();
    }

    /**
     * Проверяет корректность
     * полученного пола
     * @param str
     * @throws InvalidGenderException
     */
    public void checkGender(String str) throws InvalidGenderException{
        Gender female = Gender.Female;
        Gender male = Gender.Male;
        if (female.toString().equals(str) || male.toString().equals(str))
            System.out.println("Пол корректен");
        else
            throw new InvalidGenderException();
    }

    /**
     * Проверяет корректность
     * полученного роста
     * @param str
     * @throws InvalidHeightException
     */
    public void checkHeight(String str) throws InvalidHeightException{
        if (Double.parseDouble(str) > 0 &&
                (Pattern.matches("\\d\\.\\d\\d", str) || Pattern.matches("\\d\\.\\d", str)))
            System.out.println("Рост корректен");
        else
            throw new InvalidHeightException();
    }
}
