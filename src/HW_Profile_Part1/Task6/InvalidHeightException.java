package HW_Profile_Part1.Task6;

// Создаем класс-исключение
// для проверки корректности введенного роста
public class InvalidHeightException extends MyBaseException{

    InvalidHeightException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidHeightException(){
        // Сообщаем об ошибке
        super("Некорректный рост");
    }
}
