package HW_Profile_Part1.Task6;

// Создаем класс-исключение
// для проверки корректности введенного имя
public class InvalidNameException extends MyBaseException{

    InvalidNameException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidNameException(){
        // Сообщаем об ошибке
        super("Некорректное имя");
    }
}
