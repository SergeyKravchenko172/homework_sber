package HW_Profile_Part1.Task6;

public class Test {
    public static void main(String[] args) {
        FormValidator formValidator = new FormValidator();
        // Вводим корректные данные
        try {
            formValidator.checkName("Ivan");
            formValidator.checkBirthdate("20.02.1979");
            formValidator.checkGender("Male");
            formValidator.checkHeight("1.74");
        } catch (InvalidNameException | InvalidBirthdateException |
                InvalidGenderException | InvalidHeightException ex) {
            ex.getMessage();
        }

        // В последующих конструкциях
        // try-catch вводим
        // некорректные данные
        try {
            formValidator.checkName("ivan");
        } catch (InvalidNameException ex) {
            ex.getMessage();
        }

        try {
            formValidator.checkBirthdate("20.03.1899");
        } catch (InvalidBirthdateException ex) {
            ex.getMessage();
        }

        try {
            formValidator.checkGender("elameF");
        } catch (InvalidGenderException ex) {
            ex.getMessage();
        }

        try {
            formValidator.checkHeight("0.739");
        } catch (InvalidHeightException ex) {
            ex.getMessage();
        }
    }
}
