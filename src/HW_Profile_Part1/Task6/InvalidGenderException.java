package HW_Profile_Part1.Task6;

// Создаем класс-исключение
// для проверки корректности введенного пола
public class InvalidGenderException extends MyBaseException{

    InvalidGenderException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidGenderException(){
        // Сообщаем об ошибке
        super("Некорректный пол");
    }
}
