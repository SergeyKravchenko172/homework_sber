package HW_Profile_Part1.Task6;

// Создаем класс-исключение
// для проверки корректности введенной
// даты рождения
public class InvalidBirthdateException extends MyBaseException{

    InvalidBirthdateException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidBirthdateException(){
        // Сообщаем об ошибке
        super("Некорректная дата рождения");
    }
}
