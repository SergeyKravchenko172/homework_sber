package HW_Profile_Part1.Task6;

// Создаем базовое исключение
public class MyBaseException extends RuntimeException{

    MyBaseException(String errorMessage){
        // Сообщаем об ошибке
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
