package HW_Profile_Part1.Task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Добавили конструкцию try-catch
        // для того, чтобы уловить исключение
        try {
            int n = inputN();
            System.out.println("Успешный ввод!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    Добавили в сигнатуру throws Exception,
    так как метод может выбросить исключение
    */
    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // Изменили условие в if
        if (n > 100 || n < 0) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }
}
