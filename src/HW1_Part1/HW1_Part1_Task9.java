package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        // Получаем от пользователя бюджет мероприятия
        int n = console.nextInt();
        // Получаем бюджет от одного пользователя
        int k = console.nextInt();
        // Высчитываем максимальное количество гостей
        int visitor = n / k;
        // Выводим максимальное количество гостей
        System.out.println(visitor);
    }
}