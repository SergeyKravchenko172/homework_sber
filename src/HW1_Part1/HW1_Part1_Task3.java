package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        // Получаем имя пользователя
        String userName = console.nextLine();
        // Выводим приветствие с пользователем
        System.out.println("Привет, " + userName + "!");
    }
}