package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        // Получаем от пользователя радиус
        int radius = console.nextInt();
        // Вычисляем объем шара
        double v = 4.0 / 3.0 * Math.PI * Math.pow(radius, 3);
        // Выводим результат
        System.out.println(v);
    }
}