package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task5 {
    public static void main(String[] args) {
        final double CENTIMETRE_PER_INCH = 2.54; // сантиметров в дюйме
        Scanner console = new Scanner(System.in);
        // Получаем количество дюймов
        int inch = console.nextInt();
        // Вычисляем количество сантиметров
        double centimetre = inch * CENTIMETRE_PER_INCH;
        // Выводим результат
        System.out.println(centimetre);
    }
}