package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        // Получаем двузначное число от пользователя
        int n = console.nextInt();
        // Меняем местами цифры в введенном числе
        String result = "";
        result += n % 10;
        result += n / 10;
        // Выводим результат
        System.out.println(result);
    }
}