package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task2 {
    public static void main(String args[]){
        Scanner console = new Scanner(System.in);
        int a, b;
        // Получаем от пользователя 2 целых числа
        a = console.nextInt();
        b = console.nextInt();
        // Высчитаем среднее квадратичное этих чисел
        double s = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);
        // Выводим результат
        System.out.println(s);
    }
}