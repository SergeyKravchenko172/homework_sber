package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task6 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        final double KM_PER_MILES = 1.60934; // киллометров в миле
        // Получаем количество километров
        int km = console.nextInt();
        // Переводим километры в мили
        double miles = km / KM_PER_MILES;
        // Выводим результат
        System.out.println(miles);
    }
}