package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task4 {
    public static void main(String[] args) {
        final int SECOND_PER_MINUTES = 60; // количество секунд в минуте
        final int MINUTES_PES_HOUR = 60; // количество минут в секунде
        Scanner console = new Scanner(System.in);
        // Получаем количество секунд, пройденных за день
        int second = console.nextInt();
        // Высчитаем количество пройденных минут
        int minutes = (second / SECOND_PER_MINUTES) % SECOND_PER_MINUTES;
        // Вычисляем количество пройденных часов
        int hour = ((second / SECOND_PER_MINUTES) / MINUTES_PES_HOUR) % MINUTES_PES_HOUR;
        // Выводим текущее время
        System.out.println(hour + " " + minutes);
    }
}