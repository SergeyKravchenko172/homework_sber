package HW1_Part1;

import java.util.Scanner;

public class HW1_Part1_Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        // Получаем баланс счета в банке
        int n = console.nextInt();
        // Высчитываем дневной бюджет на 30 дней
        double result = n / 30.0;
        // Выводим результат
        System.out.println(result);
    }
}