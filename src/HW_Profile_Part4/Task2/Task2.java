package HW_Profile_Part4.Task2;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(9, 2, 5, 4, 0, 6);
        List<Integer> list1 = List.of(1, 2, 3, 4, 5);
        System.out.println(multi(list));
        System.out.println(multi(list1));
    }

    public static int multi(List<Integer> list) {
        return list.stream()
                .reduce((x, y) -> x * y)
                .get();
    }
}
