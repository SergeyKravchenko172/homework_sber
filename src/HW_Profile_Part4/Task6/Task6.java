package HW_Profile_Part4.Task6;

import java.util.*;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> set = Set.of(Set.of(2, 3, 4), Set.of(4, 5, 6), Set.of(6, 7, 8));
        System.out.println(toSetInteger(set));
    }

    public static Set<Integer> toSetInteger(Set<Set<Integer>> set) {
        return set.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}
