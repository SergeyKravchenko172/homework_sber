package HW_Profile_Part4.Task1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        System.out.println(summary());
    }

    public static int summary() {
        return IntStream
                .rangeClosed(1, 100)
                .filter(p -> p % 2 == 0)
                .sum();
    }
}
