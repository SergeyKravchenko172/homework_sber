package HW_Profile_Part4.Task4;

import java.util.List;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(12.3, 25.5, -3.8, 1.2, 17.9);
        System.out.println(sortByDecrease(list));
    }

    public static List<Double> sortByDecrease(List<Double> list) {
        return list.stream()
                .sorted(new DoubleComparator())
                .collect(Collectors.toList());
    }
}
