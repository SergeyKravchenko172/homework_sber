package HW_Profile_Part4.Task3;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> strings = List.of("abc", "", "", "def", "qqq");
        System.out.println(countNotEmptyString(strings));
    }

    public static long countNotEmptyString(List<String> list) {
        return list.stream()
                .filter(s -> !s.isEmpty())
                .count();
    }
}
