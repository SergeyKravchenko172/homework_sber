package HW_Profile_Part4.Task5;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        System.out.println(upperCaseInString(list));
    }

    public static String upperCaseInString(List<String> list) {
//        list.stream()
//                .forEach(s -> {
//                    if (list.indexOf(s) == list.size() - 1)
//                        System.out.println(s.toUpperCase(Locale.ROOT));
//                    else
//                        System.out.print(s.toUpperCase(Locale.ROOT) + ", ");
//                });
        return list.stream()
                .map(s -> s.toUpperCase(Locale.ROOT))
                .collect(Collectors.joining(", "));
    }
}
