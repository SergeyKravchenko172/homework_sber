package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task6 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить норму белков,
        // жиров, углеводов, калорий
        int A = console.nextInt();
        int B = console.nextInt();
        int C = console.nextInt();
        int K = console.nextInt();

        // Объявляем массив, хранящий потребление
        // всех нутриентов за неделю
        int[][] week = new int[7][4];

        // Получаем количество
        // употребленных нутриентов
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                week[i][j] = console.nextInt();
            }
        }

        // Объявляем переменные для хранения
        // потребления каждого нутриента
        // за неделю
        int sumA = 0;
        int sumB = 0;
        int sumC = 0;
        int sumK = 0;

        // Подсчитываем потребление
        // каждого нутриента
        // за неделю
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 7; i++) {
                switch (j){
                    case 0:
                        sumA += week[i][j];
                        break;
                    case 1:
                        sumB += week[i][j];
                        break;
                    case 2:
                        sumC += week[i][j];
                        break;
                    case 3:
                        sumK += week[i][j];
                        break;
                }
            }
        }
        // Выводим результат
        if (sumA < A && sumB < B && sumC < C && sumK < K)
            System.out.println("Отлично");
        else
            System.out.println("Нужно есть поменьше");
    }
}