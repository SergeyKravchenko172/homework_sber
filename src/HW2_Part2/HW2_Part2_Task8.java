package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить число
        int N = console.nextInt();

        // Вызвать метод и вывести результат
        System.out.println(sumNumber(N));
    }

    /**
     * Сумма цифр в числе
     * @param N
     * @return
     */
    static int sumNumber(int N){
        // Простой случай,
        // если число однозначное
        if (N < 10)
            return N;
        return N % 10 + sumNumber(N / 10);
    }
}
