package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить размер массива
        int N = console.nextInt();

        // Объявить и заполнить массив
        int[][] array = new int[N][N];
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                array[i][j] = console.nextInt();
            }
        }

        // Получить число P
        int P = console.nextInt();

        // Объявить результирующий массив
        int[][] result = new int[N - 1][N - 1];

        // Поиск столбца и строки, в которых содержится число P
        int column = -1; // искомый столбец
        int line = -1; // искомая строка
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                if (array[i][j] == P){
                    column = j;
                    line = i;
                    break;
                }
            }
            if (column >= 0 && line >= 0)
                break;
        }

        // Иницаилизация результирующего массива
        int resultI = 0; // индексы для
        int resultJ = 0; // результирующего массива
        for (int i = 0; i < N; i++) {
            try {
                if (i != line){
                    for (int j = 0; j < N; j++) {
                        if (j != column){
                            result[resultI][resultJ] = array[i][j];
                            resultJ++;
                        }
                    }
                    resultJ = 0;
                    resultI++;
                }
            } catch (ArrayIndexOutOfBoundsException ignore) { }
        }

        // Выводим результирующий массив
        for (int i = 0; i < N - 1; i++) {
            for (int j = 0; j < N - 1; j++) {
                if (j == N - 2)
                    System.out.println(result[i][j]);
                else
                    System.out.print(result[i][j] + " ");
            }
        }
    }
}
