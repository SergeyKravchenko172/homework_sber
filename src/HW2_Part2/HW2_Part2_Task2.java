package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить число столбцов и строк
        int N = console.nextInt();

        // Получить координаты точек прямоугольника
        int y1 = console.nextInt();
        int x1 = console.nextInt();
        int y2 = console.nextInt();
        int x2 = console.nextInt();

        // Объявить массив
        int[][] array = new int[N][N];

        // Заполнить массив, добавив в него прямоугольник, заполненный нулями
        for (int i = x1; i <= x2; i++) {
            for (int j = y1; j <= y2; j++) {
                if (i == x1 || i == x2 || j == y1 || j == y2)
                    array[i][j] = 1;
            }
        }

        // Отобразить массив
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++)
                if (j == N - 1)
                    System.out.println(array[i][j]);
                else
                    System.out.print(array[i][j] + " ");
        }
    }
}
