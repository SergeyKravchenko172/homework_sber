package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить размер массива
        int N = console.nextInt();

        // Объявить и заполнить массив
        int[][] array = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                array[i][j] = console.nextInt();
            }
        }

        // Проверяем симметричность массива по побочной диагонали
        boolean symmetry = false;
        for (int i = 0; i < N - 1; i++){
            for (int j = N - 1; j > i; j--){
                if (array[i][N - j - 1] == array[j][N - i - 1])
                    symmetry = true;
                else {
                    symmetry = false;
                    break;
                }
            }
            if (!symmetry)
                break;
        }
        // Выводим результат
        System.out.println(symmetry);
    }
}