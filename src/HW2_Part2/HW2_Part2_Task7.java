package HW2_Part2;

import java.util.*;


public class HW2_Part2_Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить количество участников конкурса
        int N = console.nextInt();

        // Объявляем и заполняем массив с именами хозяев
        String[] nameOwner = new String[N];
        for (int i = 0; i < N; i++) {
            nameOwner[i] = console.next();
        }

        // Объявляем и заполняем массив с именами собак
        String[] nameDog = new String[N];
        for (int i = 0; i < N; i++) {
            nameDog[i] = console.next();
        }

        // Объявляем и заполняем матрицу с баллами судей для каждого участника
        int[][] score = new int[N][3];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < 3; j++) {
                score[i][j] = console.nextInt();
            }
        }

        // Объявляем и заполняем матрциу со средним значением баллов
        double[] averageScore = new double[N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < 3; j++) {
                averageScore[i] += score[i][j];
            }
            averageScore[i] = (int) (averageScore[i] / 3.0 * 10) / 10.0;
        }



        // Ищем победителей (3 максимальные оценки)
        int indexMax1 = 0;
        for (int i = 0; i < N; i++) {
            if (averageScore[indexMax1] < averageScore[i])
                indexMax1 = i;
        }
        int indexMax2 = -1;
        for (int i = 0; i < N; i++) {
            if (indexMax2 < 0){
                if (averageScore[indexMax2 + 1] <= averageScore[i] &&
                        i != indexMax1)
                    indexMax2 = i;
            } else {
                if (averageScore[indexMax2] <= averageScore[i] &&
                        i != indexMax1)
                    indexMax2 = i;
            }
        }
        int indexMax3 = -1;
        if (N != 3){
            for (int i = 0; i < N; i++) {
                if (indexMax3 < 0){
                    if (averageScore[indexMax3 + 1] <= averageScore[i] &&
                            i != indexMax2 && i != indexMax1)
                        indexMax3 = i;
                } else {
                    if (averageScore[indexMax3] <= averageScore[i] &&
                            i != indexMax2 && i != indexMax1)
                        indexMax3 = i;
                }
            }
        } else {
            indexMax3 = 0;
            for (int i = 0; i < N; i++) {
                if (averageScore[indexMax3] > averageScore[i])
                    indexMax3 = i;
            }
        }

        // Выводим результаты
        System.out.println(nameOwner[indexMax1] + ": " +
                           nameDog[indexMax1] + ", " +
                           averageScore[indexMax1]);
        try {
            System.out.println(nameOwner[indexMax2] + ": " +
                    nameDog[indexMax2] + ", " +
                    averageScore[indexMax2]);
        } catch (ArrayIndexOutOfBoundsException ignore) { }
        try {
            System.out.println(nameOwner[indexMax3] + ": " +
                    nameDog[indexMax3] + ", " +
                    averageScore[indexMax3]);
        } catch (ArrayIndexOutOfBoundsException ignore) { }
    }
}