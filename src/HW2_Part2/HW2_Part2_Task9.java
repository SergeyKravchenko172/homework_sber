package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить число
        int N = console.nextInt();

        // Запустить метод
        // и вывести результат
        System.out.println(numbers(N));
    }

    /**
     * Печатает цифры числа
     * слева направо
     * @param N
     * @return
     */
    static String numbers(int N){
        // Простой случай,
        // если число однозначное
        if (N < 10){
            return Integer.toString(N);
        }
        return numbers(N / 10) + " " + N % 10;
    }
}