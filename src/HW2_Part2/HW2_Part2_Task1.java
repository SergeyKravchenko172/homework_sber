package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить число столбцов
        int N = console.nextInt();

        // Получить число строк
        int M = console.nextInt();

        // Объявить массив
        int[][] array = new int[M][N];

        // Заполнить массив
        for (int i = 0; i < M; i++){
            for (int j = 0; j < N; j++){
                array[i][j] = console.nextInt();
            }
        }

        // Поиск наименьшего элемента в каждой строке
        int min; // значение минимального элемента
        for (int i = 0; i < M; i++){
            min = array[i][0];
            for (int j = 0; j < N; j++){
                if (min >= array[i][j])
                    min = array[i][j];
            }
            // Выводим минимальный элемент каждой строки
            System.out.print(min + " ");
        }
    }
}
