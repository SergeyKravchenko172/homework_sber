package HW2_Part2;

import java.util.*;

public class HW2_Part2_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получить размер массива
        int N = console.nextInt();

        // Получить координаты коня на шахматной доске
        int X = console.nextInt();
        int Y = console.nextInt();

        // Запустить метод по поиску возможных ходов
        movesHorse(N, X, Y);
    }

    /**
     * Определить возможные ходы коня
     * @param N
     * @param X
     * @param Y
     * @return
     */
    static void movesHorse(int N, int X, int Y){
        // Объявить массив
        char[][] array = new char[N][N];

        // Заполнить массив нулями
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++)
                array[i][j] = '0';
        }

        // Поставить коня на шахматную доску
        array[Y][X] = 'K';

        // Определить ходы коня, обрабатывая исключение выхода за пределы массива
        try {
            array[Y - 2][X - 1] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y - 2][X + 1] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y + 2][X - 1] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y + 2][X + 1] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y - 1][X - 2] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y - 1][X + 2] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y + 1][X - 2] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }
        try {
            array[Y + 1][X + 2] = 'X';
        } catch (ArrayIndexOutOfBoundsException ignored) { }

        // Вывести шахматное поле с возможными ходами
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                if (j == N - 1)
                    System.out.println(array[i][j]);
                else
                    System.out.print(array[i][j] + " ");
            }
        }
    }
}