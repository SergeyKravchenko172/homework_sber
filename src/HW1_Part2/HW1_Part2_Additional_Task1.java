package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Additional_Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем пароль
        System.out.print("Введите пароль: ");
        String password = console.nextLine();

        // Проверяем количество символов в пароле
        if (password.length() < 8){
            System.out.println("пароль не прошел проверку");
        } else {
            int countBig = 0; // Подсчет заглавных букв
            int countLittle = 0; // Подсчет строчных букв
            int countNumber = 0; // Подсчет цифр
            int countSpecialSymbol = 0; // Подсчет специальных символов
            for (int i = 0; i < password.length(); i++){
                if (password.charAt(i) >= 65 && password.charAt(i) <= 90){
                    ++countBig; // Если символ - заглавная буква
                } else if (password.charAt(i) >= 97 && password.charAt(i) <= 122){
                    ++countLittle; // Если символ - строчная буква
                } else if (password.charAt(i) >= 48 && password.charAt(i) <= 57){
                    ++countNumber; // Если символ - цифра
                } else if (password.charAt(i) == 95 || password.charAt(i) == 42
                        || password.charAt(i) == 45){
                    ++countSpecialSymbol; // Если символ - специальный символ
                }
            }

            // Проверяем, были ли выполнены все условия
            if (countBig > 0 && countLittle > 0 && countNumber > 0
                    && countSpecialSymbol > 0){
                System.out.println("пароль надежный");
            } else {
                System.out.println("пароль не прошел проверку");
            }
        }
    }
}