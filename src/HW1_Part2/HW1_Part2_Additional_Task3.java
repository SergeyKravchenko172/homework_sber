package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Additional_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем модель телефона
        String model = console.nextLine();
        String modelPhone = model.substring(0, model.indexOf(" "));

        // Получаем стоимость телефона
        int price = console.nextInt();

        // Проверяем, соответствует ли условиям телефон
        if ((modelPhone.equals("samsung") || modelPhone.equals("iphone"))
                && (price >= 50000 && price <= 120000)){
            System.out.println("Можно купить");
        } else {
            System.out.println("Не подходит");
        }
    }
}