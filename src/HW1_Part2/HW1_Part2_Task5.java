package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем коэффициенты квадратного уравнения
        System.out.print("Введите коэффициенты квадратного уравнения: ");
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();

        // Проверяем, есть ли решение у квадратного уравнения
        // (дискриминант должен быть положительным)
        if ((Math.pow(b, 2) - 4 * a * c) < 0)
            System.out.println("Решения нет");
        else
            System.out.println("Решение есть");
    }
}