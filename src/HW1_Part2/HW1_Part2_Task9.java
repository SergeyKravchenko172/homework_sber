package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем число x
        System.out.print("Введите число x: ");
        int x = console.nextInt();

        // Проверяем, выполняется ли тождество (sin^2(x) + cos^2(x) - 1 = 0)
        boolean b = (Math.round(Math.sin(x) * Math.sin(x) + Math.cos(x) * Math.cos(x)) - 1 == 0);
        System.out.println(b);
    }
}