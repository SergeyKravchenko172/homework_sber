package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task6 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем количество выученных иностранных слов
        System.out.print("Введите количество выученнных иностранных слов: ");
        int count = console.nextInt();

        // Проверяем уровень владения языком
        if (count < 500) System.out.println("beginner");
        else if (count < 1000) System.out.println("pre-intermediate");
        else if (count < 2500) System.out.println("intermediate");
        else if (count < 3500) System.out.println("upper-intermediate");
        else System.out.println("fluent");
    }
}