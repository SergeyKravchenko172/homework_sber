package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем оценки Пети
        System.out.print("Введите оценки Пети: ");
        int score_1 = console.nextInt();
        int score_2 = console.nextInt();
        int score_3 = console.nextInt();

        // Проверяем, уменьяшаются ли монотонно оценки
        if (score_1 > score_2 && score_2 > score_3){
            // Если оценки монотонно уменьшаются, то выводим, что пора трудиться
            System.out.println("Петя, пора трудиться");
        } else {
            // Если оценки не уменьшаются монотонно, то хвалим Петю
            System.out.println("Петя молодец!");
        }
    }
}