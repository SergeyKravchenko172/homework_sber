package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем время в часах
        System.out.print("Введите количество часов: ");
        int hour = console.nextInt();

        // Проверяем, пора ли Пете на обед
        if (hour > 12) System.out.println("Пора");
        else System.out.println("Рано");
    }
}