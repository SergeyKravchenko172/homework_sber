package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем порядковый номер дня недели
        System.out.print("Введите порядковый номер дня недели: ");
        int day = console.nextInt();

        // Проверяем по порядковому номеру, сколько дней до выходных
        if (day >= 6) System.out.println("Ура, выходные!");
        else System.out.println(6 - day);
    }
}