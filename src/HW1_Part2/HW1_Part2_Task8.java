package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем строчку
        System.out.print("Введите строчку: ");
        String str = console.nextLine();

        // Выводим две строчки, полученные из входной разделением по последнему пробелу
        System.out.println(str.substring(0, str.lastIndexOf(" ")));
        System.out.println(str.substring(str.lastIndexOf(" ") + 1, str.length()));
    }
}