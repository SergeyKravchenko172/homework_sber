package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task11 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Полуаем длины сторон треугольника
        System.out.print("Введите длины сторон треугольника: ");
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();

        // Проверяем, можно ли составить их этих сторон треугольник
        // Треугольник можно составить, если сумма длин двух сторон больше третьей
        boolean check = (a + b) > c && (a + c) > b && (b + c) > a;
        System.out.println(check);
    }
}