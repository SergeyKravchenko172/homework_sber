package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Additional_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем содержимое посылки
        System.out.print("Введите содержимое посылки: ");
        String mailPackage = console.nextLine();

        // Проверяем посылку на наличие содержимого
        if (mailPackage.equals("")) {
            System.out.println("все ок");
        } else {
            // Проверяем наличие и камней, и запрещенной продукции
            if ((mailPackage.startsWith("запрещенная продукция") ||
                    mailPackage.endsWith("запрещенная продукция")) &&
                    (mailPackage.startsWith("камни") ||
                            mailPackage.endsWith("камни"))) {
                System.out.println("в посылке камни и запрещенная продукция");
                // Проверяем наличие только запрещенной продукции без камней
            } else if ((mailPackage.startsWith("запрещенная продукция") ||
                    mailPackage.endsWith("запрещенная продукция")) &&
                    !(mailPackage.startsWith("камни") ||
                            mailPackage.endsWith("камни"))) {
                System.out.println("в посылке запрещенная продукция");
                // Проверяем наличие только камней без запрещенной продукции
            } else if (!(mailPackage.startsWith("запрещенная продукция") ||
                    mailPackage.endsWith("запрещенная продукция")) &&
                    (mailPackage.startsWith("камни") ||
                            mailPackage.endsWith("камни"))) {
                System.out.println("камни в посылке");
            } else {
                System.out.println("все ок");
            }
        }
    }
}