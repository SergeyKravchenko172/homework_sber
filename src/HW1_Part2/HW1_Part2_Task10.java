package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task10 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем число n
        System.out.print("Введите число n: ");
        double n = console.nextDouble();

        // Проверяем, выполняется ли тождество log(e^n) == n
        boolean b = Math.log(Math.pow(Math.E, n)) == n;
        System.out.println(b);
    }
}