package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем строчку
        System.out.print("Введите строчку: ");
        String str = console.nextLine();

        // Выводим две строчки, полученные из входной разделением по первому пробелу
        System.out.println(str.substring(0, str.indexOf(" ")));
        System.out.println(str.substring(str.indexOf(" ") + 1, str.length()));
    }
}