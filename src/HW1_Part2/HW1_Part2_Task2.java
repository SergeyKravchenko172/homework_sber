package HW1_Part2;

import java.util.Scanner;

public class HW1_Part2_Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        // Получаем две координаты точки
        System.out.print("Введите координаты точки: ");
        int coordinate_x = console.nextInt();
        int coordinate_y = console.nextInt();

        // Проверяем, находиться ли точка в первом квадранте
        if (coordinate_x > 0 && coordinate_y > 0){
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}