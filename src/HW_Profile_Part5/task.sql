CREATE TABLE flows_list (
    id SERIAL PRIMARY KEY,
    flow VARCHAR(30) NOT NULL,
    price INTEGER NOT NULL
);

INSERT INTO flows_list(flow, price)
VALUES('Роза', 100);

INSERT INTO flows_list(flow, price)
VALUES('Лилия', 50);

INSERT INTO flows_list(flow, price)
VALUES('Ромашка', 25);

SELECT * FROM flows_list;

CREATE TABLE client (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    phone_number BIGINT CHECK (phone_number >= 79000000000 AND phone_number <= 79999999999)
);

INSERT INTO client(name, phone_number)
VALUES('Иван', 79109109110);

INSERT INTO client(name, phone_number)
VALUES('Петр', 79245064730);

INSERT INTO client(name, phone_number)
VALUES('Василий', 79990091920);

INSERT INTO client(name, phone_number)
VALUES('Виктор', 79538961322);

SELECT * FROM client;

CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    client_id INTEGER REFERENCES client,
    flow_id INTEGER REFERENCES flows_list,
    count INTEGER NOT NULL CHECK (count > 0 AND count <= 1000),
    sum INTEGER NOT NULL,
    date_of_creation TIMESTAMP
);

INSERT INTO orders(client_id, flow_id, count, sum, date_of_creation)
VALUES (1, 1, 10, 1000, NOW());

INSERT INTO orders(client_id, flow_id, count, sum, date_of_creation)
VALUES (2, 1, 5, 500, '2022-10-12');

INSERT INTO orders(client_id, flow_id, count, sum, date_of_creation)
VALUES (1, 2, 12, 600, NOW());

INSERT INTO orders(client_id, flow_id, count, sum, date_of_creation)
VALUES (3, 3, 5, 125, '2022-12-01 12:30');

SELECT * FROM orders;

SELECT * FROM orders
JOIN client b on orders.client_id = b.id
WHERE orders.id = 1;

SELECT * FROM orders WHERE client_id = 3 AND date_of_creation >= NOW() - INTERVAL '1 MONTH';
SELECT * FROM orders WHERE client_id = 2 AND date_of_creation >= NOW() - INTERVAL '1 MONTH';
SELECT * FROM orders WHERE client_id = 1 AND date_of_creation >= NOW() - INTERVAL '1 MONTH';

SELECT flow, count FROM flows_list
JOIN orders o on flows_list.id = o.flow_id
WHERE count = (SELECT MAX(count) FROM orders);

SELECT SUM(sum) FROM orders;